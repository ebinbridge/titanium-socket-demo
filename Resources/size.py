import os, glob,sys,json
import Image
i = 0
result  = []
search_extensions = ['png']
def find_text_in_files(path):
	global i
	global result
	if path == "":
		search = "*"
	else:
		search = "/*"	
	for f in glob.glob(path+search):
		if os.path.isdir(f):
			find_text_in_files(f)
		else:
			if f.split(".")[-1] not in search_extensions:
				continue
			i = i+1
			#print i
			print f
			path = 	os.path.dirname(f)
			im = Image.open(f)
			im_size = list(im.size)
			im_data = {'size':im_size,'path':path}
			result.append(im_data)
			#im.close()
			#print list(im_size)
			#print f

if __name__ == '__main__':
	find_text_in_files(sys.argv[1])
	#im = Image.open(result[0]['path'])
	result = json.dumps(result)
	print result
	open('imgs.json','w').write(result)			