/*config*/
var api_url = "http://54.254.249.63:8080/first";
var socket_url = "54.254.249.63:8080";
var img_dir = "img/";
/*end config*/

Titanium.UI.setBackgroundColor('#000');
var db = Titanium.Database.open('news');
db.execute('CREATE TABLE IF NOT EXISTS news(id INTEGER PRIMARY KEY AUTOINCREMENT, news TEXT, serverid INTEGER UNIQUE, createdAt datetime);');
db.execute("DELETE FROM news WHERE createdAt < (SELECT DATETIME('now', '-3 day'))");
var datas = [];
var header="";
var even_row = true;


var io = require('socket');
try{
    socket = io.connect(socket_url);
    socket.on('connect', function () {});
    socket.on('users_updated', function (data) {
    	 table.data = [];
    	 table.appendRow(header);
       datas = [];
       data = saveToDb(data);
       loadTable(data);
         
    });
}
catch(err){
    console.log("could not access server socket");
}



var tabGroup = Titanium.UI.createTabGroup({
    activeTabBackgroundColor:'#A01111',
});

var win1 = Titanium.UI.createWindow({  
    title:'Tab 1',
    backgroundColor:'#fff'
});

var tab1 = Titanium.UI.createTab({  
  
    title:'Today',
    window:win1
});

var win2 = Titanium.UI.createWindow({  
    title:'Tab 2',
    backgroundColor:'#fff'
});

var tab2 = Titanium.UI.createTab({  
    
    title:'Y Day',
    backgroundColor : '#A01111',
    activeTabBackgroundColor:'#A01111',
    window:win2
});
var win3 = Titanium.UI.createWindow({  
    title:'Tab 3',
    backgroundColor:'#fff'
});

var tab3 = Titanium.UI.createTab({  
   
    title:'Old',
    window:win3
});


var textfield = Titanium.UI.createTextField();
var table = Titanium.UI.createTableView({});
var table2 = Titanium.UI.createTableView({});
var table3 = Titanium.UI.createTableView({});
var row = Titanium.UI.createTableViewRow({height:50,backgroundImage:img_dir+"football-net.jpg"});

header =row;
table.appendRow(row); 
table2.appendRow(row); 
table3.appendRow(row); 

win1.add(table);
win2.add(table2);
win3.add(table3);

tabGroup.addTab(tab1);  
tabGroup.addTab(tab2);
tabGroup.addTab(tab3);   


// open tab group
tabGroup.open();

get_data();
function get_data(){

 var url = api_url;
 var client = Titanium.Network.createHTTPClient({
     onload : function(e) {
           serverData = JSON.parse(this.responseText);
           saveToDb(serverData);
           serverData = getFromDb();
           loadTable(serverData);
     },
     onerror : function(e) {},
     timeout : 5000 
 });
 client.open("GET", url);
 client.send();
}
 
 
 function get_data_poll(){

 var url = api_url;
 var client = Titanium.Network.createHTTPClient({
     onload : function(e) {
           serverData = JSON.parse(this.responseText);
           data = saveToDb(serverData);
           loadTable(data);
     },
     onerror : function(e) {},
     timeout : 5000 
 });
 client.open("GET", url);
 client.send();
}


setInterval(get_data_poll,1000*60*2);

function loadTable(searverData){
	 for (var i = 0; i < searverData.length; i++){
        
        if (even_row){ bgColor = "#F0FFF0"; even_row = false;}
        else { bgColor = "#E4FFE4";even_row = true;}
	    var row = Titanium.UI.createTableViewRow({height:46,backgroundColor: bgColor});
	    
        var take_button = Titanium.UI.createImageView({
            image:img_dir+'add.png',
            top:9,
            right:'7',
            width:32,
            height:28,
            myrow:row,
            myid:datas.length,
            serverid:searverData[i].id,
       }); 
	   take_button.addEventListener('click', function(e){
            var details = Ti.UI.createWindow({
                url:'details.js',
                backgroundColor:'#eee',
                navBarHidden : false
            });
            details.data = datas[e.source.myid];
            console.log(details.data);
            details.open();
       });
	      
	  var title = Titanium.UI.createLabel({
        text:searverData[i].nickname.substring(0,80)+"",
        left:8,
        width:'85%',
        top:3,
        bottom:3,
        textAlign:'left',
        color:"#111",
        });
	  row.add(title);
      row.add(take_button);
      //console.log(searverData[i]);
      if(searverData[i].diffDays ==0){
       table.insertRowAfter(0,row); 
      }
      else if(searverData[i].diffDays ==1){
        table2.insertRowAfter(0,row); 
      }
      else if(searverData[i].diffDays ==2){
        
        table3.insertRowAfter(0,row); 
      }
	  
	  datas.push(searverData[i]);

        
    }
}


function saveToDb(searverData){
    result = [];
    var oneDay = 24*60*60*1000;
    var ids = [];
    var news_data = db.execute("SELECT * FROM news WHERE createdAt > (SELECT DATETIME('now', '-30 day'))");
    while (news_data.isValidRow()){
        var first = new Date();
        var second = new Date(news_data.fieldByName('createdAt'));
        var diffDays = Math.round(Math.abs((first.getTime() - second.getTime())/(oneDay)));
        ids.push(news_data.fieldByName('serverid'));
        news_data.next();
    }
    console.log(ids);
    for (var i = 0; i < searverData.length; i++){
        if(ids.indexOf(searverData[i].id) == -1){
            db.execute('INSERT OR IGNORE INTO news (news,serverid,createdAt) VALUES (?,?,?)', searverData[i].nickname, searverData[i].id, searverData[i].createdAt);
            tmp = {};
            var first = new Date();
            var second = new Date(searverData[i].createdAt);
            tmp.diffDays = Math.round(Math.abs((first.getTime() - second.getTime())/(oneDay)));
            tmp.createdAt = second;
            tmp.nickname = searverData[i].nickname;
            tmp.id = searverData[i].id;
            result.push(tmp);


        }
    }

    return result;

}



function getFromDb(){
    result = [];
    var oneDay = 24*60*60*1000;
    var news_data = db.execute("SELECT * FROM news WHERE createdAt > (SELECT DATETIME('now', '-30 day'))");
    while (news_data.isValidRow()){
        tmp = {};
        var first = new Date();
        var second = new Date(news_data.fieldByName('createdAt'));
        tmp.diffDays = Math.round(Math.abs((first.getTime() - second.getTime())/(oneDay)));
        tmp.createdAt = second;
        tmp.nickname = news_data.fieldByName('news');
        tmp.id = news_data.fieldByName('serverid');
        result.push(tmp);
        news_data.next();
    }
    return result;
}



