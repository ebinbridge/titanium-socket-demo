var details = Ti.UI.currentWindow;
var table_det = Ti.UI.createTableView({
    top: '15dp',
    left: '10dp',
    right: '10dp',
    backgroundColor: '#E4FFE4',
    borderColor: '#57DC4E',
    borderRadius: 8,
    height: '280dp',
    shadow: {
        shadowRadius:4,
        shadowOpacity:0.2,
        shadowOffset:{x:0, y:0}
    }
});
function add_data_row (data_key,data_val,back_color) {
        var back = {even:"#999",odd:"#eee"};
      
         var label_desc = Ti.UI.createLabel({
          text:data_val,
          color:'#032900',
          right:'5%',
          width:"90%",
          top:"10%",
          font: {
            fontSize:'18dp'
          },
         });

      var row_det = Ti.UI.createTableViewRow({height:"100dp"});
   
      row_det.add(label_desc);
      table_det.appendRow(row_det); 
}
add_data_row("name",details.data.nickname,"even");
details.add(table_det);